import java.util.Scanner;

public class Driver{
	public static void main(String[] args){
		Scanner scan = new Scanner(System.in);
		
		System.out.println("Greetings user");
		System.out.println("Input a number");
		int firstNumber = scan.nextInt();		
		System.out.println("Input a second number");
		int secondNumber = scan.nextInt();
		
		System.out.println(Calculator.sum(firstNumber, secondNumber));
		
		System.out.println("Input a number to find the square root");
		int rootNumber = scan.nextInt();
		
		System.out.println(Calculator.squareRoot(rootNumber));
		
		System.out.println("Here's a random number");
		System.out.println(Calculator.random());
	}
}