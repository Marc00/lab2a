import java.util.Random;

public class Calculator{
	public static int sum(int a, int b){
		return a + b;
	}
	
	public static double squareRoot(int a){
		return Math.sqrt(a);
	}
	
	public static int random(){
		Random rand = new Random();
		return rand.nextInt();
	}
	
	public static double divide(int numerator, int denominator){
		if(denominator != 0){
			return numerator/denominator;
		} else{
			return 0;
		}
	}
}